<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Task;
use SebastianBergmann\Environment\Console;

class TaskController2 extends Controller
{
    public function index(Request $req)
    {


        return view('index', [
            'tasks' => Task::all(),
            'count' => Task::count(),
            'page' => 'task'
        ]);
    }

    public function task_active()
    {
        return view('active', [
            'tasks' => Task::where('status', 'active')->get(),
            'count' => Task::where('status', 'active')->count(),
            'page' => 'active'
        ]);
    }

    public function task_completed()
    {
        return view('completed', [
            'tasks' => Task::where('status', 'completed')->get(),
            'count' => Task::where('status', 'completed')->count(),
            'page' => 'completed'
        ]);
    }

    public function add(Request $req)
    {
        Task::create([
            'isi' => $req->isi,
            'status' => 'active'
        ]);

        return redirect()->back();
    }

    public function ubah_status(Request $req)
    {
        $task = Task::find($req->id);
        $task->status = $req->status;
        $task->save();
    }

    public function activate()
    {
        Task::where('status', 'active')->update([
            'status' => 'completed'
        ]);

        return redirect()->back();
    }

    public function delete($id)
    {
        Task::findOrFail($id)->delete();

        return redirect()->back();
    }

    public function delete_all()
    {
        Task::where('status', 'completed')->delete();

        return redirect()->back();
    }
}
