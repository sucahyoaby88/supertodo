<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Task;
use Illuminate\Support\Facades\Validator;

class TaskController extends Controller
{
    public function index()
    {
        return response()->json([
            'task' => Task::all()
        ]);
    }

    public function store(Request $req)
    {
        $validasi = Validator::make($req->all(), [
            'isi' => 'required|string|max:100'
        ]);

        if($validasi->fails()){
            return response()->json($validasi->errors());
        }
        
        $task = Task::create([
            'isi' => $req->isi,
            'status' => 'active'
        ]);

        if($task){
            return response()->json('Task Berhasil Ditambahkan');
        }else{
            return response()->json('Task Gagal Ditambahkan');
        }
    }

    public function update(Request $req, $id)
    {
        $validasi = Validator::make($req->all(), [
            'isi' => 'required|string|max:100'
        ]);

        if($validasi->fails()){
            return response()->json($validasi->errors());
        }
        
        $task = Task::where('id', $id)->update([
            'isi' => $req->isi,
            'status' => 'active'
        ]);

        if($task){
            return response()->json('Task Berhasil Diubah');
        }else{
            return response()->json('Task Gagal Diubah');
        }
    }

    // public function delete($id)
    // {
    //     $task = Task::where('id', $id)->delete();

    //     if($task){
    //         return response()->json('Task Berhasil Dihapus');
    //     }else{
    //         return response()->json('Task Gagal Dihapus');
    //     }
    // }


    public function activate($id)
    {
        $task = Task::where('id', $id)->first();

        if($task->status == "active"){
            return response()->json('Task Status Sudah Active');
        }else{
            $task->update([
                'status' => 'active'
            ]);
            return response()->json('Task Status Kembali ke Active');
        }
    }

    public function complete($id)
    {
        $task = Task::where('id', $id)->first();

        if($task->status == "completed"){
            return response()->json('Task Status Sudah Completed');
        }else{
            $task->update([
                'status' => 'completed'
            ]);
            return response()->json('Task Status Berubah ke Completed');
        }
    }

    public function complete_all()
    {
        $task = Task::where('status', 'active')->update([
            'status' => 'completed'
        ]);

        if($task){
            return response()->json('Semua Task Active Berhasil Diubah ke Completed');
        }else{
            return response()->json('Semua Task Active Gagal Diubah ke Completed');
        }
    }

    public function delete_all_completed()
    {
        $task = Task::where('status', 'completed')->delete();

        if($task){
            return response()->json('Semua Task Completed Berhasil Dihapus');
        }else{
            return response()->json('Semua Task Completed Gagal Dihapus');
        }
    }

    public function show_active()
    {
        $jumlah_active = Task::where('status', 'active')->count();

        if($jumlah_active > 0){
            return response()->json([
                'jumlah task active' => $jumlah_active,
                'task active' => Task::where('status', 'active')->get()
            ]);
        }else{
            return response()->json('Tidak ada task active');
        }
    }

    public function show_completed()
    {
        $jumlah_completed = Task::where('status', 'completed')->count();

        if($jumlah_completed > 0){
            return response()->json([
                'jumlah task completed' => $jumlah_completed,
                'task completed' => Task::where('status', 'completed')->get()
            ]);
        }else{
            return response()->json('Tidak ada task completed');
        }
    }
}
