<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use App\Models\Task;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        // \App\Models\User::factory(10)->create();

        Task::create([
            'isi' => 'Buat aplikasi',
            'status' => 'active'
        ]);

        Task::create([
            'isi' => 'Buat database',
            'status' => 'completed'
        ]);
    }
}
