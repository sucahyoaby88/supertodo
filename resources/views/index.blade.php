<!doctype html>
<html lang="en">
	<head>
		<meta charset="utf-8">
		<meta name="viewport" content="width=device-width, initial-scale=1">
		<title>Template • Todo</title>
		<link rel="stylesheet" href="/css/base.css">
		<link rel="stylesheet" href="/css/index.css">
		<!-- CSS overrides - remove if you don't need it -->
	</head>
	<body>
		<section class="todoapp">
			<header class="header">
				<h1>Super2Do</h1>
				<form action="add/" method="post">
					@csrf
					<input type="text" class="new-todo" name="isi" placeholder="What needs to be done?" autofocus>
					<button type="submit">Add task</button>
				</form>
			</header>
			<!-- This section should be hidden by default and shown when there are todos -->
			<section class="main">
				<input id="toggle-all" class="toggle-all" type="checkbox">
				<ul class="todo-list">
					<!-- These are here just to show the structure of the list items -->
					<!-- List items should get the class `editing` when editing and `completed` when marked as completed -->
					@if (!empty($tasks))
						
						@foreach ($tasks as $task)
						
						<li data-id="{{ $task->id }}" class="{{ ($task->status == "completed") ? 'completed' : ''}}">
							<div class="view">
								<input id={{ $task->id }} class="toggle" type="checkbox" 
								{{ ($task->status == "completed") ? 'checked' : ''}} onclick="handleClick({{ $task->id }})">
								
								<label>{{ $task->isi }}</label>
								<a href="delete/{{ $task->id }}"><button class="destroy"></button></a>
							</div>
						</li>
						
						@endforeach

					@endif
				</ul>
			</section>
			<!-- This footer should be hidden by default and shown when there are todos -->
			<footer class="footer">
				<!-- This should be `0 items left` by default -->
				<span class="todo-count"><strong>{{ $count }}</strong> item left</span>
				<!-- Remove this if you don't implement routing -->
				<ul class="filters">
					<li>
						<a class="{{ ($page === "task") ? 'selected' : ''}}" href="/">All</a>
					</li>
					<li>
						<a class="{{ ($page === "active") ? 'selected' : ''}}" href="/active">Active</a>
					</li>
					<li>
						<a  class="{{ ($page === "completed") ? 'selected' : ''}}" href="/completed">Completed</a>
					</li>
				</ul>
				<!-- Hidden if no completed items are left ↓ -->
			</footer>
		</section>
		<!-- Scripts here. Don't remove ↓ -->
		<script src="/js/app.js"></script>
		<script src="https://code.jquery.com/jquery-3.6.0.min.js" integrity="sha256-/xUj+3OJU5yExlq6GSYGSHk7tPXikynS7ogEvDej/m4=" crossorigin="anonymous"></script>
		<script>
				function handleClick(id) {
		let isChecked = document.getElementById(id).checked ? 'completed' : 'active'
		let currentEl = document.querySelector(`li[data-id="${id}"]`);

		if (isChecked === 'completed') {
			currentEl.classList.add("completed")
		} else {
			currentEl.classList.remove("completed")
		}

		$.ajax({
			type : "GET",
			dataType : "json",
			url : "ubahstatus/",
			data : {
				'status' : isChecked, 'id' : id
			}
		});
	}
		</script>
	</body>
</html>
