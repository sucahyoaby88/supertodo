<?php

use Illuminate\Support\Facades\Route;
use App\Http\Controllers\TaskController2;


/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

// Route::get('/', function () {
//     return view('index');
// });

Route::get('/', [TaskController2::class, 'index']);
Route::get('active', [TaskController2::class, 'task_active']);
Route::get('completed', [TaskController2::class, 'task_completed']);

Route::post('add', [TaskController2::class, 'add']);

Route::get('ubahstatus', [TaskController2::class, 'ubah_status']);

Route::get('activate', [TaskController2::class, 'activate']);

Route::get('delete/{id}', [TaskController2::class, 'delete']);
Route::get('clear', [TaskController2::class, 'delete_all']);
