<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;
use App\Http\Controllers\TaskController;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:sanctum')->get('/user', function (Request $request) {
    return $request->user();
});

Route::get('task', [TaskController::class, 'index']);
Route::get('task_active', [TaskController::class, 'show_active']);
Route::get('task_completed', [TaskController::class, 'show_completed']);

Route::post('create_task', [TaskController::class, 'store']);
Route::put('update_task/{id}', [TaskController::class, 'update']);
// Route::delete('delete_task/{id}', [TaskController::class, 'delete']);

Route::put('activate_task/{id}', [TaskController::class, 'activate']);
Route::put('complete_task/{id}', [TaskController::class, 'complete']);

Route::put('complete_all_task/', [TaskController::class, 'complete_all']);
Route::delete('delete_completed_task/', [TaskController::class, 'delete_all_completed']);